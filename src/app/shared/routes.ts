import { RouterModule, Routes } from '@angular/router';
import { NavbarComponent } from '../components/navbar/navbar.component';


export const routes: Routes = [
  { path: 'app', component: NavbarComponent},
  { path: '', redirectTo:'app',pathMatch:'full' }
]